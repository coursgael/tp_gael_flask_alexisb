# Utiliser une image officielle de Python comme image de base
FROM python:3.10

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

COPY . /app

# Installer les dépendances nécessaires
RUN pip install --no-cache-dir -r requirements.txt

# Copier le reste du code de l'application dans le répertoire de travail
COPY . .

# Exposer le port 5001 pour accéder à l'application Flask
EXPOSE 5001

# Définir la commande par défaut pour exécuter l'application
CMD ["python", "Flask/main.py"]
