from flask import Flask, jsonify

app = Flask(__name__)

# Route to return the constant number in HTML format
@app.route('/constant', methods=['GET'])
def constant_html():
    constant_number = '9525'
    html_content = f'''
    <!DOCTYPE html>
    <html>
    <head>
        <title>Constant Number</title>
        <style>
            body {{
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                margin: 0;
            }}
            #response {{
                font-size: 3em;
                font-weight: bold;
            }}
        </style>
    </head>
    <body>
        <div id="response">{constant_number}</div>
    </body>
    </html>
    '''
    return html_content

# Route to return the constant number in JSON format
@app.route('/api/constant', methods=['GET'])
def constant_json():
    constant_number = '9525'
    return jsonify({'constant': constant_number})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
